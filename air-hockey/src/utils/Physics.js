import Matter from "matter-js";


const Physics = (entities, { touches, time }) => {
    let engine = entities.physics.engine;
    let disk = entities.disk.body;
    let pusher = entities.topPusher.body;
    
    touches.filter(t => t.type === "move").forEach(t => {
        console.log("im t",t)
        if(Math.round(t.event.pageX) === Math.round(pusher.position.x) && Math.round(t.event.pageY) === Math.round(pusher.position.y)){
           Matter.Body.translate(pusher,{x:t.delta.pageX,y:t.delta.pageY})

        }
        
    })

    /*touches.filter(t => t.type === "press").forEach(t => {
    	if(Math.round(t.event.pageX) === Math.round(disk.position.x) && Math.round(t.event.pageY) === Math.round(disk.position.y)){
    		Matter.Body.applyForce( disk, disk.position, {x: 0.00, y: -0.10});

    	}

    	//Matter.Body.applyForce( disk, disk.position, {x: 0.10, y: -0.10});
    
        
    });*/

    Matter.Engine.update(engine, time.delta);

    return entities;
};

export default Physics;