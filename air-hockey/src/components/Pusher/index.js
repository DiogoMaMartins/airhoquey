import React, { Component } from "react";
import { View } from "react-native";

export default class Pusher extends Component {
    render() {
        const width = this.props.size[0];
        const height = this.props.size[1];
        const x = this.props.body.position.x - width / 2;
        const y = this.props.body.position.y - height / 2;

        return (
            <View
                style={{
                    position: "absolute",
                    left: x,
                    top: y,
                    width: width,
                    height: height,
                    borderRadius: width/2,
                    backgroundColor: this.props.color,
                    zIndex:4
                }} />
    );
  }
}