import React, { Component } from 'react';
import { StyleSheet, View,Dimensions } from 'react-native';
import Matter from "matter-js";
import { GameEngine } from "react-native-game-engine";
import Disk from '../../components/Disk';
import Wall from '../../components/Wall';
import Constants from '../../utils/Constants';
import Physics from '../../utils/Physics';
import Goal from '../../components/Goal';
import Pusher from '../../components/Pusher';

 

 const diskSettings = {
      inertia: 0,
      friction: 0,
      frictionStatic: 0,
      frictionAir: 0,
      restitution: 1
    };


    const GAME_WIDTH = 650;
    const GAME_HEIGHT = 340;

 const wallSettings = {
      isStatic: true
    };

 const screenHeight = Math.round(Dimensions.get('window').height);


export default class Game extends Component {
    constructor(props){
        super(props);

        this.state = {
            running: true
        };

        this.gameEngine = null;

        this.entities = this.setupWorld();
    }

   setupWorld = () => {
        const screenHeight = Math.round(Dimensions.get('window').height);
        let engine = Matter.Engine.create({ enableSleeping: false });

      
        let world = engine.world;
        let disk  = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT / 2, 50, 50);
        
       /*  let disk = Matter.Bodies.circle(
       Constants.MAX_WIDTH / 2 - 50,
       Constants.MAX_WIDTH / 2 ,
       50,
      {
        ...diskSettings,
        label: "disk"
      }
    );*/

    let topPusher = Matter.Bodies.circle(Constants.MAX_WIDTH / 2,280,50,{isStatic:true}) ;
    let leftWall = Matter.Bodies.rectangle(0, 200, 30, screenHeight+screenHeight, { isStatic: true });
    let rightWall =  Matter.Bodies.rectangle(Constants.MAX_WIDTH, screenHeight, 30, screenHeight+screenHeight, { isStatic: true });

    

        //let leftWall = Matter.Bodies.rectangle( Constants.MAX_WIDTH -25, Constants.MAX_HEIGHT - 55, Constants.MAX_WIDTH, 50, { isStatic: true });
        let floor = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, Constants.MAX_HEIGHT - 25, Constants.MAX_WIDTH, 50, { isStatic: true });
        let goalFloor =  Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2,500, Constants.MAX_WIDTH, 0, { isStatic: true,isSensor: true, });
        
        let ceiling = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2,2, Constants.MAX_WIDTH, 50, { isStatic: true });
        let goal = Matter.Bodies.rectangle( Constants.MAX_WIDTH / 2, 25, Constants.MAX_WIDTH, 0 , { isStatic:true });



          Matter.Events.on(engine, 'collisionStart', function(event) {

          let pairs = event.pairs;

          for (var i = 0; i < pairs.length; i++) {
            var pair = pairs[i];

        
        
        }
        console.log("test",pairs[0].bodyB,"test")
          //console.log("im pairs",pairs)
        })







        Matter.World.add(world, [disk,topPusher, floor, ceiling,goal,goalFloor,leftWall,rightWall]);

        return {
            physics: { engine: engine, world: world },
            disk: { body: disk, size: [50, 50], color: 'red', renderer: Disk},
            topPusher:{body:topPusher, size: [80,80], color:'green', renderer:Pusher},
            floor: { body: floor, size: [Constants.MAX_WIDTH, 50],rotate:"-90deg", color: "teal", renderer: Wall },
            ceiling: { body: ceiling, size: [Constants.MAX_WIDTH, 50],rotate:"90deg", color: "teal", renderer: Wall },
            goal: { body: goal, size: [Constants.MAX_WIDTH, 50],rotate:"90deg", color: "black", renderer: Goal },
            goalFloor: { body: goalFloor,  size: [Constants.MAX_WIDTH, 50],rotate:"-90deg", color: "black", renderer: Goal },
            
            leftWall:{body:leftWall,size: [30,1300],color: "teal", renderer: Wall },
            rightWall:{body:rightWall,size: [30,screenHeight + screenHeight],color: "teal", renderer: Wall },

        }
    }




    render() {
  

        return (
            <View style={styles.container}>
                <GameEngine
                  ref={(ref) => { this.gameEngine = ref; }}
                  style={styles.gameContainer}
                  running={this.state.running}
                  systems={[Physics]}
                  entities={this.entities}>

                  
                </GameEngine>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    gameContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        borderColor:'teal',
        borderWidth:4
    },
});