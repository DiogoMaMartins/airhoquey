import React from 'react';
import { View, Text,TouchableOpacity,Image,ImageBackground } from 'react-native';
import { Audio } from 'expo-av';
import AwesomeButton from "react-native-really-awesome-button";
import { Ionicons,MaterialIcons,Entypo,MaterialCommunityIcons } from '@expo/vector-icons';

class HomeScreen extends React.Component {

	async componentWillMount() {
  this.backgroundMusic = new Audio.Sound();
  try {
    await this.backgroundMusic.loadAsync(
      require("../../../sounds/opening.mp3")
    );
    await this.backgroundMusic.setIsLoopingAsync(true);
    await this.backgroundMusic.playAsync();
    // Your sound is playing!
  } catch (error) {
  	console.log("im error sound",error)
    // An error occurred!
  
}
}

async play(){
	 this.backgroundMusic = new Audio.Sound();
  try {
    await this.backgroundMusic.loadAsync(
      require("../../../sounds/opening.mp3")
    );
    await this.backgroundMusic.setIsLoopingAsync(true);
    await this.backgroundMusic.playAsync();
    // Your sound is playing!
  } catch (error) {
  	console.log("im error sound",error)
    // An error occurred!
  
}
}
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
        <ImageBackground source={require('../../../assets/hockeyBoy.png')} style={{width: '100%', height: '100%',alignItems: 'center', justifyContent: 'space-around',}}>
        <Image style={{marginTop:'25%'}} source={require('../../../assets/airHoquey.png')}/>

      	<View style={{display:'flex',height:'40%',alignItems: 'center', justifyContent: 'space-around' }}>
	      	<AwesomeButton   borderRadius={25} textColor="#fff"  backgroundDarker="#f18f27" borderColor="#F16129" backgroundColor="#F16129"  width={200} onPress={() => this.props.navigation.navigate('Game')}>
	      	 
	      	 <Ionicons name="md-person" size={32} color="white" style={{marginRight:20}}/>
     		 <Text style={{color:'white'}}>1 Player</Text>
    		</AwesomeButton>

    		<AwesomeButton   borderRadius={25} textColor="#fff"  backgroundDarker="#f18f27" borderColor="#F16129" backgroundColor="#F16129"  width={200}>
	      	 
	      	 <MaterialIcons name="group" size={32} color="white" style={{marginRight:20}}/>
     		 <Text style={{color:'white'}}> 2 Players</Text>
    		</AwesomeButton>


    		<AwesomeButton   borderRadius={25} textColor="#fff"  backgroundDarker="#f18f27" borderColor="#F16129" backgroundColor="#F16129"  width={200}>
	      	 
	      	 <MaterialCommunityIcons name="lan-connect" size={32} color="white" style={{marginRight:20}}/>
     		 <Text style={{color:'white'}}> Multiplayer</Text>
    		</AwesomeButton>

	     	
     	</View>


     	<View style={{display:'flex', flexDirection:'row',width:'90%',alignItems: 'center', justifyContent: 'space-between' }}>
     	<AwesomeButton   borderRadius={25} textColor="#fff"  backgroundDarker="#f18f27" borderColor="#F16129" backgroundColor="#F16129" width={50} height={50} >
	      	 
	      	 <Ionicons name="md-settings" size={32} color="white"/>
    		</AwesomeButton>

    		<AwesomeButton   borderRadius={25} textColor="#fff"  backgroundDarker="#f18f27" borderColor="#F16129" backgroundColor="#F16129" width={50} height={50} >
	      	 
	      	 <Entypo name="shopping-cart" size={32} color="white" />
    		</AwesomeButton>

     	</View>
     	</ImageBackground>
      </View>
    );
  }
}

export default HomeScreen;