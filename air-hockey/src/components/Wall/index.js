import React, { Component } from "react";
import { View } from "react-native";
import Goal from '../Goal';

export default class Wall extends Component {
    render() {
        const width = this.props.size[0];
        const height = this.props.size[1];
        const x = this.props.body.position.x - width / 2;
        const y = this.props.body.position.y - height / 2;
        const rotate = this.props.rotate;

        return (
            <View
                style={{
                    position: "absolute",
                    left: x,
                    top: y,
                    width: width,
                    height: height,
                    backgroundColor: this.props.color
                }} >
                {/*<Goal size={[width,height]} rotate={rotate}/>*/}
                </View>
    );
  }
}