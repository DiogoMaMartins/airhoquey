import React from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Game from '../src/screens/Game';
import HomeScreen from '../src/screens/Homescreen';


const AppNavigator = createStackNavigator({
	Game:{
    screen:Game,
  },
  Home: {
    screen: HomeScreen,
  },
  
},{
  headerMode:'none'
});

export default createAppContainer(AppNavigator);