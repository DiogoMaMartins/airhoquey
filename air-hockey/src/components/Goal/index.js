import React, { Component } from "react";
import { View } from "react-native";

export default class Goal extends Component {
    render() {
        const width = this.props.size[0];
        const height = this.props.size[1];
        const rotate = this.props.rotate;
        const x = this.props.body.position.x - width / 2;
        const y = this.props.body.position.y - height / 2;

        return (
            <View
                style={{
                   width: 160,
                   height: 160,
                   left: x,
                   top: y,
                   backgroundColor:this.props.color,
                   alignSelf:'center',
                   borderTopRightRadius: 120, 
                   borderBottomRightRadius: 120,
                   transform: [{ rotate: rotate }]
                
                }} />
    );
  }
}
